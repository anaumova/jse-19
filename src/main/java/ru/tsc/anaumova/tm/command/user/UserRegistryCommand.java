package ru.tsc.anaumova.tm.command.user;

import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry";

    public static final String DESCRIPTION = "Registry new user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }

}
