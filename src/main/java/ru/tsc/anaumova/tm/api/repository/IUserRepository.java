package ru.tsc.anaumova.tm.api.repository;

import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}