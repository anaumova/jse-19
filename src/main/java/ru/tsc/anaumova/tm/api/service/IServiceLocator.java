package ru.tsc.anaumova.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    IUserService getUserService();

    IAuthService getAuthService();

}