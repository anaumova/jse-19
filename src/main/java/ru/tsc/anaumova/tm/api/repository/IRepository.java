package ru.tsc.anaumova.tm.api.repository;

import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void clear();

    int getSize();

    boolean existsById(String id);

}