package ru.tsc.anaumova.tm.api.repository;

import ru.tsc.anaumova.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}