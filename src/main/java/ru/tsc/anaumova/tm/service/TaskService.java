package ru.tsc.anaumova.tm.service;

import ru.tsc.anaumova.tm.api.repository.ITaskRepository;
import ru.tsc.anaumova.tm.api.service.ITaskService;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.anaumova.tm.exception.field.EmptyDescriptionException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyNameException;
import ru.tsc.anaumova.tm.exception.field.IncorrectIndexException;
import ru.tsc.anaumova.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.create(name);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        return repository.create(name, description);
    }

    @Override
    public Task create(
            final String name,
            final String description,
            final Date dateBegin,
            final Date dateEnd
    ) {
        final Task task = create(name, description);
        if (task == null) throw new TaskNotFoundException();
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(projectId);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= getSize()) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= getSize()) throw new IncorrectIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}