package ru.tsc.anaumova.tm.service;

import ru.tsc.anaumova.tm.api.repository.IUserRepository;
import ru.tsc.anaumova.tm.api.service.IUserService;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.exception.entity.UserNotFoundException;
import ru.tsc.anaumova.tm.exception.field.*;
import ru.tsc.anaumova.tm.model.User;
import ru.tsc.anaumova.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        return repository.create(login, password);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        return repository.create(login, password, email);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        return repository.create(login, password, role);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = repository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = repository.findOneByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = findByEmail(email);
        return remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findOneById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

}