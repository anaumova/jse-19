package ru.tsc.anaumova.tm.exception.entity;

import ru.tsc.anaumova.tm.exception.AbstractException;

public class ModelNotFoundException extends AbstractException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}